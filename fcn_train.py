import argparse
import os
import subprocess
import tensorflow as tf
import datetime as dt
import random
import pickle
import timeit

from neuralnet import fcn2vgg, calculate_loss
from persistence import ConfigObject, DataStore, InputReader
from tensorflow.python.platform import gfile


def train_model(options, data_store):
    input_reader = InputReader(options)
    output_data = {'train_losses': [], 'test_losses': []}

    train_start_time = timeit.default_timer()

    # Compute progress step
    batch_count = (
        input_reader.total_images / options.batch_size if input_reader.total_images >= options.batch_size else 1)
    total_iterations = batch_count * options.training_epochs
    progress_step = max(float(total_iterations) / options.progress_image_count, 1)

    with tf.variable_scope('FCN_VGG'):
        # Data placeholders
        inputBatchImages = tf.placeholder(dtype=tf.float32,
                                          shape=[None, options.image_height, options.image_width,
                                                 options.image_channels],
                                          name="inputBatchImages")
        inputBatchLabels = tf.placeholder(dtype=tf.float32,
                                          shape=[None, options.image_height, options.image_width, options.num_classes],
                                          name="inputBatchLabels")
        inputKeepProbability = tf.placeholder(dtype=tf.float32, name="inputKeepProbability")

    vgg_fcn = fcn2vgg.FCN2VGG(batch_size=options.batch_size,
                              stats_file=options.stats_file_name,
                              tensor_board=options.tensorboard_visualization,
                              activation=options.activation)

    with tf.name_scope('Model'):
        noise_stddev = tf.placeholder(dtype=tf.float32, name='noiseStddev')

        # Construct model
        vgg_fcn.build(rgb=inputBatchImages,
                      keep_probability=inputKeepProbability,
                      noise_stddev=noise_stddev,
                      num_classes=options.num_classes,
                      random_init_fc8=True,
                      debug=(options.verbose > 0))

    with tf.name_scope('Loss'):
        # Define loss
        loss = calculate_loss(vgg_fcn.softmax, inputBatchLabels, options.num_classes)

    with tf.name_scope('Optimizer'):
        # Define Optimizer
        # optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)
        optimizer = tf.train.AdamOptimizer(learning_rate=options.learning_rate)

        # Op to calculate every variable gradient
        gradients = tf.gradients(loss, tf.trainable_variables())
        gradients = list(zip(gradients, tf.trainable_variables()))
        # Op to update all variables according to their gradient
        applyGradients = optimizer.apply_gradients(grads_and_vars=gradients)

    # Initializing the variables
    init = tf.global_variables_initializer()

    if options.tensorboard_visualization:
        # Create a summary to monitor cost tensor
        tf.summary.scalar("loss", loss)

        # Create summaries to visualize weights
        for var in tf.trainable_variables():
            tf.summary.histogram(var.name, var)
        # Summarize all gradients
        for grad, var in gradients:
            tf.summary.histogram(var.name + '/gradient', grad)

        # Merge all summaries into a single op
        mergedSummaryOp = tf.summary.merge_all()

    # 'Saver' op to save and restore all the variables
    saver = tf.train.Saver()

    step = 1

    with tf.Session() as sess:
        if options.load_model:
            print("Loading")

            # Restore Graph
            with gfile.FastGFile(options.graph_dir + options.output_graph_name, 'rb') as f:
                graphDef = tf.GraphDef()
                graphDef.ParseFromString(f.read())
                sess.graph.as_default()
                tf.import_graph_def(graphDef, name='')

                print("Graph Loaded")

            saver = tf.train.Saver(tf.global_variables())  # defaults to saving all variables - in this case w and b

            # Restore Model
            ckpt = tf.train.get_checkpoint_state(options.checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                print("Model loaded Successfully!")
            else:
                print("Model not found")
                exit()

        if options.start_training_from_scratch:
            data_store.create_session_directories()

        # Restore checkpoint
        else:
            ckpt = tf.train.get_checkpoint_state(options.checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                print(ckpt.model_checkpoint_path)
                print("Checkpoint loaded Successfully!")
            else:
                print("Checkpoint not found")
                exit()

            # Restore iteration number
            nameComps = ckpt.model_checkpoint_path.split('-')
            step = int(nameComps[1])
            input_reader.restore_checkpoint(step)
            lastSaveStep = step

        if options.tensorboard_visualization:
            # Op for writing logs to Tensorboard
            summary_writer = tf.summary.FileWriter(options.logs_dir, graph=tf.get_default_graph())

        print("Starting network training")
        sess.run(init)

        # Save progress train and test image
        progress_train_index = random.randint(0, input_reader.total_images - 1)
        progress_test_index = random.randint(0, input_reader.total_images_test - 1)

        progress_train_img = input_reader.get_train_batch_by_indices([progress_train_index])[0][0]
        progress_test_img = input_reader.get_test_batch_by_indices([progress_test_index])[0][0]

        data_store.save_initial_progress_image(progress_train_img, is_train=True)
        data_store.save_initial_progress_image(progress_test_img, is_train=False)

        # Keep training until reach max iterations
        while True:
            batchImagesTrain, batchLabelsTrain = input_reader.get_train_batch()

            # If training iterations completed
            if batchImagesTrain is None:
                print("Training completed")
                break

            noise_stddev_value = random.uniform(options.min_std_dev, options.max_std_dev)
            print("Gaussian noise stddev: ", noise_stddev_value)

            # Run optimization op (backprop)
            if options.tensorboard_visualization:
                _, summary = sess.run([applyGradients, mergedSummaryOp],
                                      feed_dict={inputBatchImages: batchImagesTrain,
                                                 inputBatchLabels: batchLabelsTrain,
                                                 inputKeepProbability: options.neuron_alive_probability,
                                                 noise_stddev: noise_stddev_value})

                # Write logs at every iteration
                summary_writer.add_summary(summary, step)
            else:
                _, summary = sess.run([applyGradients],
                                      feed_dict={inputBatchImages: batchImagesTrain,
                                                 inputBatchLabels: batchLabelsTrain,
                                                 inputKeepProbability: options.neuron_alive_probability,
                                                 noise_stddev: noise_stddev_value})

            # Compute runtime
            runtime = timeit.default_timer() - train_start_time

            if step == 1 or step % options.display_step == 0:
                # Calculate batch loss
                if options.display_step_dont_save_images:
                    [trainLoss] = sess.run([loss], feed_dict={inputBatchImages: batchImagesTrain,
                                                              inputBatchLabels: batchLabelsTrain,
                                                              inputKeepProbability: 1,
                                                              noise_stddev: 0})
                else:
                    # always False...
                    [trainLoss, trainImagesProbabilityMap] = sess.run([loss, vgg_fcn.probabilities],
                                                                      feed_dict={inputBatchImages: batchImagesTrain,
                                                                                 inputBatchLabels: batchLabelsTrain,
                                                                                 inputKeepProbability: 1,
                                                                                 noise_stddev: 0})

                    # Save image results
                    # print("Saving images")
                    # input_reader.save_last_batch_results(trainImagesProbabilityMap, is_train=True)

                # print "Iter " + str(step) + ", Minibatch Loss= " + "{:.6f}".format(trainLoss)
                print("Iter " + str(step) + ", Minibatch Loss= ", trainLoss)
                output_data['train_losses'].append((step, trainLoss))

                # Write loss to file
                # with open(loss_path_train, 'a') as f:
                #    f.write("%s=%s=%s\n" % (step, runtime, trainLoss))

            # Save progress labels
            if (step - 1) % progress_step < 1:
                batch_images_progress, batch_labels_progress = input_reader.get_train_batch_by_indices(
                    [progress_train_index])
                trainImagesProbabilityMap = sess.run(vgg_fcn.probabilities,
                                                     feed_dict={inputBatchImages: batch_images_progress,
                                                                inputBatchLabels: batch_labels_progress,
                                                                inputKeepProbability: 1,
                                                                noise_stddev: 0})

                data_store.save_progress_outputs(trainImagesProbabilityMap, step, is_train=True)

                batch_images_progress, batch_labels_progress = input_reader.get_test_batch_by_indices([progress_test_index])
                testImagesProbabilityMap = sess.run(vgg_fcn.probabilities,
                                                    feed_dict={inputBatchImages: batch_images_progress,
                                                               inputBatchLabels: batch_labels_progress,
                                                               inputKeepProbability: 1,
                                                               noise_stddev: 0})

                data_store.save_progress_outputs(testImagesProbabilityMap, step, is_train=False)

            # Increase step
            step += 1

            if step % options.save_step == 0:
                # Save model weights to disk
                saver.save(sess, options.checkpoint_dir + 'model.ckpt', global_step=step)
                print("Model saved in file: %s" % options.checkpoint_dir)
                lastSaveStep = step

            # Check the accuracy on test data
            if step == 2 or step % options.evaluate_step == 0:
                # Report loss on test data
                batch_images_test, batch_labels_test = input_reader.get_test_batch()

                if options.evaluate_step_dont_save_images:
                    [test_loss] = sess.run([loss], feed_dict={inputBatchImages: batch_images_test,
                                                              inputBatchLabels: batch_labels_test,
                                                              inputKeepProbability: 1,
                                                              noise_stddev: 0})
                    print("Test loss:", test_loss)

                else:
                    [test_loss, testImagesProbabilityMap] = sess.run([loss, vgg_fcn.probabilities],
                                                                     feed_dict={inputBatchImages: batch_images_test,
                                                                                inputBatchLabels: batch_labels_test,
                                                                                inputKeepProbability: 1,
                                                                                noise_stddev: 0})
                    print("Test loss:", test_loss)

                    # Save image results
                    # print("Saving images")
                    # input_reader.save_last_batch_results(testImagesProbabilityMap, is_train=False)

                output_data['test_losses'].append((step - 1, test_loss))

        # Save final model weights to disk
        saver.save(sess, options.checkpoint_dir + 'model.ckpt', global_step=step)
        print("Model saved in file: %s" % options.checkpoint_dir)
        lastSaveStep = step

        # Write Graph to file
        print("Writing Graph to File")
        os.system("rm -rf " + options.graph_dir)
        tf.train.write_graph(sess.graph_def, options.graph_dir, options.input_graph_name, as_text=False)  # proto

        # We save out the graph to disk, and then call the const conversion routine.
        input_graph_path = options.graph_dir + options.input_graph_name
        input_checkpoint_path = options.checkpoint_dir + 'model.ckpt' + '-' + str(lastSaveStep)

        output_node_names = "Model/probabilities"
        # restoreOpName = "save/restore_all"
        # fileNameTensorName = "save/Const:0"
        output_graph_path = options.graph_dir + options.output_graph_name
        # clearDevices = True

        # TODO: Be careful, had to change this to python3, because python is running python 2.7
        cmd = "python3 freeze_graph.py --input_graph " + input_graph_path + \
              " --input_checkpoint " + input_checkpoint_path + " --output_graph " + output_graph_path + \
              " --output_node_names " + output_node_names + " --input_binary"

        print(cmd)
        process = subprocess.Popen(cmd, shell=True)
        process.wait()

        # Report loss on train data
        batchImagesTrain, batchLabelsTrain = input_reader.get_train_batch_by_indices(
            range(min(input_reader.total_images, options.batch_size)))

        trainLoss = sess.run(loss, feed_dict={inputBatchImages: batchImagesTrain, inputBatchLabels: batchLabelsTrain,
                                              inputKeepProbability: 1,
                                              noise_stddev: 0})

        if output_data['train_losses'][-1][0] != step - 1:
            output_data['train_losses'].append((step, trainLoss))
        print("Train loss (current):", trainLoss)

        # Report loss on test data
        batch_images_test, batch_labels_test = input_reader.get_test_batch()

        test_loss = sess.run(loss, feed_dict={inputBatchImages: batch_images_test, inputBatchLabels: batch_labels_test,
                                              inputKeepProbability: 1,
                                              noise_stddev: 0})

        if output_data['test_losses'][-1][0] != step - 1:
            output_data['test_losses'].append((step - 1, test_loss))
        print("Test loss (current):", test_loss)

        # Measure time
        train_end_time = timeit.default_timer()
        train_duration = train_end_time - train_start_time
        print("Optimization Finished!")
        print("Train duration: %s" % train_duration)

        # Write data to File for later analysis by fcn_evaluate.py
        output_data['train_duration'] = train_duration
        output_data['progress_train_image'] = input_reader.image_list[progress_train_index][0]
        output_data['progress_test_image'] = input_reader.image_list_test[progress_test_index][0]
        output_data['iterations'] = step
        output_data['progress_step'] = progress_step

        output_data['train_path'] = options.train_path
        output_data['test_path'] = options.test_path
        output_data['image_height'] = options.image_height
        output_data['image_width'] = options.image_width
        output_data['image_channels'] = options.image_channels
        output_data['batch_size'] = options.batch_size
        output_data['train_epochs'] = options.training_epochs
        output_data['learning_rate'] = options.learning_rate
        output_data['stddev'] = [options.min_std_dev, options.max_std_dev]
        output_data['from_scratch'] = options.start_training_from_scratch
        output_data['activation'] = options.activation

        output_data['train_total_images'] = input_reader.total_images
        output_data['test_total_images'] = input_reader.total_images_test

        output_file_path = os.path.join(options.session_directory,
                                        options.data_directory,
                                        options.session_timestamp + '.p')
        print("Save output data:", output_file_path)

        pickle.dump(output_data, open(output_file_path, 'wb'))
        pickle.dump(output_data, open(os.path.join(options.session_directory, 'latest_data.p'), 'wb'))


def test_model(options):
    input_reader = InputReader()
    print("Testing saved Graph")
    output_graph_path = options.graph_dir + options.output_graph_name
    # Now we make sure the variable is now a constant, and that the graph still produces the expected result.
    with tf.Session() as session:
        output_graph_def = tf.GraphDef()
        with open(output_graph_path, "rb") as f:
            output_graph_def.ParseFromString(f.read())
            session.graph.as_default()
            _ = tf.import_graph_def(output_graph_def, name="")

        # Print all variables in graph
        print("Printing all variable names")
        all_vars = output_graph_def.node
        for node in all_vars:
            print(node.name)

        print("done!")

        # sess.run(tf.initialize_all_variables())
        output_node = session.graph.get_tensor_by_name("Model/probabilities:0")
        inputBatchImages = session.graph.get_tensor_by_name("FCN_VGG/inputBatchImages:0")
        inputKeepProbability = session.graph.get_tensor_by_name("FCN_VGG/inputKeepProbability:0")
        noise_stddev = session.graph.get_tensor_by_name("Model/noiseStddev:0")

        batch_images_test, batch_labels_test = input_reader.get_test_batch()
        batch_images_test = batch_images_test[0:1, :, :, :]
        start_time = dt.datetime.now()
        output = session.run(output_node, feed_dict={inputBatchImages: batch_images_test,
                                                     inputKeepProbability: 1,
                                                     noise_stddev: 0})

        print("Output shape:", output.shape)
        end_time = dt.datetime.now()

        print("Time consumed in executing graph:", ((end_time.microsecond - start_time.microsecond) / 1e6))

    print("Graph tested")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--trainPath', type=str, default='', help='Directory to Train Data')
    parser.add_argument('--testPath', type=str, default='', help='Directory to Test Data')
    args = parser.parse_args()

    opt = ConfigObject("config/config.cfg", args.trainPath)
    opt['train_path'] = args.trainPath
    opt['test_path'] = args.testPath
    opt = opt.parse_configuration()

    ds = DataStore(opt)

    if opt.train_model:
        train_model(opt, ds)

    if opt.test_model:
        test_model(opt)
