import tensorflow as tf


def inverse_square_relu(x, name, alpha=1.0):
    # calculate case x < 0: x / sqrt(1 + a * x**2)
    lower_zero = x / tf.sqrt(1.0 + tf.constant(alpha) * x ** 2.0)
    return tf.maximum(x, lower_zero, name=name)  # value of lower_zero will always be smaller than x


def parametric_relu(x, name):
    param = tf.Variable(0.0, name=name + '_param')
    return tf.maximum(param * x, x, name=name)


def leaky_relu(x, name, alpha=0.01):
    return tf.maximum(alpha * x, x, name=name)


activation = {'relu': tf.nn.relu,
              'elu': tf.nn.elu,
              'lrelu': leaky_relu,
              'prelu': parametric_relu,
              'isrelu': inverse_square_relu}


def get_activation(mode, features, name):
    mode = mode.lower()
    if mode not in activation:
        raise Exception("'%s' activation function not implemented." % mode)

    return activation[mode](features, name + '_' + mode)


if __name__ == '__main__':

    init_op = tf.global_variables_initializer()

    with tf.Session() as sess:
        for v in [-10., -5., -0.3, 0., 2., 4.]:
            x = tf.constant(v)
            y = get_activation('ISRELU', x, "conv_1")
            print("isrelu", y.eval())
