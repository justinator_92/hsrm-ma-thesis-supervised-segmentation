import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import argparse
import tensorflow as tf
import numpy as np
import skimage.io
import skimage.color
import timeit
import os
import pickle
import io

from collections import OrderedDict
from persistence import InputReader, PDFWriter, util, ConfigObject

SEPARATOR = '-' * 100  # Used to separate stdout result sections
MEASURE_LABELS = np.array(["Pixel Accuracy", "Mean Accuracy", "Mean IU", "Frequency Mean IU"])


def get_tensors(graph, graph_def, name="", start="", end=""):
    """ Filter tensors by name or/and start/end characters """
    node_names = [node.name for node in graph_def.node]
    node_names = list(filter(lambda n: (not name or n == name)
                                       and (not start or n.lower().startswith(start.lower()))
                                       and (not end or n.lower().endswith(end.lower())), node_names))

    tensors = [graph.get_tensor_by_name("%s:%s" % (node_name, 0)) for node_name in node_names]
    return tensors, node_names


def visualize_layer_outputs(tensor_outputs, count=3):
    prepared_plots = []

    figure = plt.figure(frameon=False, figsize=(5, 5))
    grid_spec = gridspec.GridSpec(1, 2)
    grid_spec.update(wspace=0, hspace=0)  # set the spacing between axes.

    for output in tensor_outputs:
        count = min(output.shape[0], count)
        per_image_visualization = []

        for i in range(0, count):
            # Greyscale
            ax = plt.subplot(grid_spec[0])
            ax.set_axis_off()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            ax.imshow(output[i, :, :, 0], interpolation="nearest", cmap="gray")  # Filter 0
            ax.autoscale_view('tight')

            # Color
            ax = plt.subplot(grid_spec[1])
            ax.set_axis_off()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            ax.imshow(output[i, :, :, 0], interpolation="nearest")  # Filter 0
            ax.autoscale_view('tight')

            # figure.savefig("test.png", format='jpg', bbox_inches='tight', pad_inches=0)
            # plt.show()

            image_mem = io.BytesIO()
            figure.savefig(image_mem, format='jpg', bbox_inches='tight', pad_inches=0)
            image_mem.seek(0)

            per_image_visualization.append(image_mem)

            figure.clf()

        prepared_plots.append(per_image_visualization)

    return prepared_plots


def visualize_progress(image_path, label_paths):
    image = skimage.io.imread(image_path)
    labels = [skimage.io.imread(p) for p in label_paths]
    merged_labels = util.merge_images(labels, 10)

    image_mem = util.array_as_mem_image(image)
    labels_mem = util.array_as_mem_image(merged_labels)

    return image_mem, labels_mem


def eval_batch(batch_input, batch_masks, batch_result):
    """ Evaluates a single batch by comparing the ground truth labels with the output of the CNN """
    # calculates metrics
    pixel_accuracy = np.sum(np.logical_and(batch_result, batch_masks)) / np.sum(batch_masks)

    mean_accuracy = util.calculate_measure_for_batch("mean_accuracy", batch_masks, batch_result)
    mean_iu = util.calculate_measure_for_batch("mean_iu", batch_masks, batch_result)
    freq_mean_iu = util.calculate_measure_for_batch("freq_mean_iu", batch_masks, batch_result)

    measures = np.array([pixel_accuracy, mean_accuracy, mean_iu, freq_mean_iu])
    print(measures)

    # convert input_image, input_mask and output_result to images as BytesIO
    visualizations = []

    for image, mask, result in zip(batch_input, batch_masks, batch_result):
        images = [io.BytesIO() for _ in range(4)]

        mask = util.convert_one_hot_to_gray_image(mask)
        result = util.convert_one_hot_to_gray_image(result)

        pos_neg_image = util.create_positive_negative_image(mask, result)

        for i, img in enumerate([image, mask, result, pos_neg_image]):
            skimage.io.imsave(images[i], img)
            images[i].seek(0)

        visualizations.append(images)

    return measures, visualizations


def eval_segmentation(options):
    """ Evaluates the CNN based segmentation approach """
    output_graph_path = options.graph_dir + options.output_graph_name
    input_reader = InputReader(options, shuffle_images=True)

    start_time = timeit.default_timer()

    with tf.Session() as session:
        # Load graph
        output_graph_def = tf.GraphDef()
        with open(output_graph_path, "rb") as f:
            output_graph_def.ParseFromString(f.read())
            session.graph.as_default()
            _ = tf.import_graph_def(output_graph_def, name="")

        output_node = session.graph.get_tensor_by_name("Model/probabilities:0")
        input_batch_images = session.graph.get_tensor_by_name("FCN_VGG/inputBatchImages:0")
        input_keep_probability = session.graph.get_tensor_by_name("FCN_VGG/inputKeepProbability:0")
        noise_stddev = session.graph.get_tensor_by_name("Model/noiseStddev:0")

        # Total measurements of all iterations
        measurements_per_source = OrderedDict()
        visualizations_per_source = OrderedDict()
        image_names_per_source = OrderedDict()
        total_measurements = np.zeros(4)

        # In each iteration a random batch of x images is feed into the CNN
        image_sources = input_reader.get_image_sources()
        for source in image_sources:
            print("Evaluation image source: %s" % source)
            image_count = len(input_reader.image_test_list[source])
            batch_count = image_count / options.batch_size

            measurements_per_source[source] = np.zeros(4)

            visualizations = []
            image_names = []

            # Take a batch of images+labels(masks)
            for i, (batch_images, batch_labels, batch_image_names) in enumerate(
                    input_reader.iterate_ordered_test_batches(source, options.eval_per_source)):
                # Forward pass of image batch into CNN
                batch_output = session.run(output_node, feed_dict={input_batch_images: batch_images,
                                                                   input_keep_probability: 1,
                                                                   noise_stddev: 0})

                batch_output = util.convert_image_batch_to_one_hot(batch_output)

                print(SEPARATOR)
                print("Batch %s out of %s" % (i + 1, batch_count))
                print(SEPARATOR)

                # Increase total measurements by batch evaluation results
                measures, batch_concat = eval_batch(batch_images, batch_labels, batch_output)
                measurements_per_source[source] += measures
                visualizations.extend(batch_concat)
                image_names.extend([img[0] for img in batch_image_names])

                print(SEPARATOR)
                print('Total evaluation results of source %s' % source)
                print("%s of %s images processed" % (options.batch_size * (i + 1), options.batch_size * batch_count))
                print("Progress: %s%%" % ((float(i + 1) / batch_count) * 100))
                print(SEPARATOR)
                print("")

            # add different visualizations for [trainTeeth, testTeeth]
            visualizations_per_source[source] = visualizations[:options.results_per_source]
            image_names_per_source[source] = image_names[:options.results_per_source]

            # calculate average of measurements
            measurements_per_source[source] = np.copy(measurements_per_source[source]) / options.results_per_source
            total_measurements += measurements_per_source[source]

        train_data = pickle.load(open(os.path.join(options.session_directory, options.output_data_name), 'rb'))

        # Take a random batch of images and visualize the layer outputs
        tensors, tensor_names = get_tensors(graph=session.graph,
                                            graph_def=session.graph_def,
                                            start='Model/conv',
                                            end=train_data['activation'].lower())  # convlayer name ends with activation

        random_images = input_reader.get_random_test_batch()[0]

        vis = session.run(tensors, feed_dict={input_batch_images: random_images, noise_stddev: 0})

        visualized_outputs = visualize_layer_outputs(vis, 3)
        layer_visualization_images = [io.BytesIO() for _ in range(3)]
        for mem, img in zip(layer_visualization_images, random_images[:3]):
            skimage.io.imsave(mem, img)

        # Stop timer
        end_time = timeit.default_timer()
        evaluation_duration = end_time - start_time
        print("Elapsed time: %s" % evaluation_duration)

        metadata = dict(graph_dir=options.graph_dir, graph_name=options.output_graph_name,
                        image_width=options.image_height, image_height=options.image_height,
                        image_channels=options.image_channels,
                        test_data=options.test_data, evaluation_duration=evaluation_duration)

        train_progress = visualize_progress(input_reader.get_train_progress_image(options.session_directory),
                                            input_reader.get_train_progress_labels(options.session_directory))

        test_progress = visualize_progress(input_reader.get_test_progress_image(options.session_directory),
                                           input_reader.get_test_progress_labels(options.session_directory))

        source_examples = {}
        for source in image_sources:
            source_images = input_reader.get_source_images(source, 15)
            merged = util.merge_images(source_images, 5).astype(np.uint8)
            source_examples[source] = util.array_as_mem_image(merged)

        train_loss_function = train_data['train_losses']
        test_loss_function = train_data['test_losses']

        # create PDF output file
        pdf_writer = PDFWriter(os.path.join(options.session_directory, options.session_timestamp + '.pdf'))

        pdf_writer.create_metadata_section(metadata, train_data)
        pdf_writer.create_measurements_table("Overall evaluation results", total_measurements / 2, MEASURE_LABELS)
        pdf_writer.create_loss_functions(train_loss_function, test_loss_function)
        pdf_writer.create_all_segmentation_examples(visualizations_per_source, image_names_per_source,
                                                    measurements_per_source, source_examples, MEASURE_LABELS)

        pdf_writer.create_image_progress_history(train_progress[0], train_progress[1],
                                                 "Segmentation progress on train data")
        pdf_writer.create_image_progress_history(test_progress[0], test_progress[1],
                                                 "Segmentation progress on test data")

        pdf_writer.create_conv_layer_outputs(list(zip(visualized_outputs, tensor_names)), layer_visualization_images)
        pdf_writer.build_file()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--trainPath', default='', type=str, help="Current path to train data")
    parser.add_argument('--trainData', default='', type=str, help="Path to train data directory")
    parser.add_argument('--testData', default='', type=str, help="Path to test data directory")
    args = parser.parse_args()

    opt = ConfigObject('config/config.cfg', args.trainPath)
    opt['test_data'] = OrderedDict([('trainTeeth', args.trainData), ('testTeeth', args.testData)])
    opt['train_path'] = args.trainPath

    eval_segmentation(opt.parse_configuration())
    print("<- Finished testing segmentation ->")
