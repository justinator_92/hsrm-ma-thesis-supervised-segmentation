import os
import numpy as np
import skimage
import skimage.io
import skimage.color
import skimage.transform
from persistence import util
import random

from collections import OrderedDict


TRAIN_PROGRESS_DIRECTORY = 'trainProgress'
TEST_PROGRESS_DIRECTORY = 'testProgress'

TRAIN_EVAL_DIRECTORY = 'trainEval'
TEST_EVAL_DIRECTORY = 'testEval'


class InputReader:

    def __init__(self, options, shuffle_images=False):
        self.options = options

        self.image_list = util.get_image_file_names(self.options.train_path)

        try:
            self.image_list_test = util.get_image_file_names(self.options.test_path)
        except AttributeError:  # if self.options.testPath is empty...
            self.image_list_test = []

        try:
            self.image_test_list = OrderedDict()
            for k, path in options.test_data.items():
                self.image_test_list[k] = util.get_image_file_names(path)
        except AttributeError:
            self.image_test_list = OrderedDict()

        if shuffle_images:
            for v in self.image_test_list.values():
                random.shuffle(v)

        print("Train Data size: ", len(self.image_list))
        print("Test Data size: ", len(self.image_list_test))

        self.current_index = 0
        self.total_epochs = 0
        self.total_images = len(self.image_list)
        self.total_images_test = len(self.image_list_test)

        self.last_indices = []
        self.all_train_indices = []
        self.all_test_indices = []
        self.tested_images = []
        if self.image_test_list:
            self.source_indices = {k: [] for k in self.get_image_sources()}

    def read_images_from_disc(self, image_mask_files):
        """Consumes a list of filenames and returns image with mask
        Args:
          image_mask_files: List of image files [image, mask]
        Returns:
          Two 4-D numpy arrays: The input images as well as well their corresponding binary mask
        """
        images = []
        masks = []
        for image_path, mask_image_path in image_mask_files:
            if self.options.verbose > 1:
                print("Image path:", image_path)
                print("Mask path:", mask_image_path)

            # Read image
            img = skimage.io.imread(image_path)
            img = np.stack([img, img, img], axis=2)

            img = skimage.transform.resize(img, (self.options.image_height, self.options.image_width),
                                           preserve_range=True).astype(np.uint8)
            images.append(img)

            # Read mask
            mask = skimage.io.imread(mask_image_path)
            mask = skimage.transform.resize(mask, (self.options.image_height, self.options.image_width),
                                            preserve_range=True).astype(np.uint8)

            if self.options.num_classes == 2:
                # Convert the mask to [H, W, options.numClasses]
                # Temporary check for mask < 128 and mask >= 128
                # The mask does not only contain full black or white pixels, maybe
                # because of the png format.
                # TODO Change to a equal 0 resp. 255 comparison when masks are saved correctly
                background_class = (mask < 128).astype(np.uint8)
                foreground_class = (mask >= 128).astype(np.uint8)

                mask = np.stack([background_class, foreground_class], axis=2)

            if self.options.num_classes == 4:
                c1 = (mask <= 43).astype(np.uint8)
                c2 = ((mask > 43) & (mask <= 128)).astype(np.uint8)
                c3 = ((mask > 128) & (mask <= 213)).astype(np.uint8)
                c4 = (mask > 213).astype(np.uint8)

                mask = np.stack([c1, c2, c3, c4], axis=2)

            masks.append(mask)

        # Convert list to numpy array
        images = np.array(images)
        masks = np.array(masks)
        return images, masks

    def get_train_batch_by_indices(self, indices):
        image_batch, mask_batch = self.read_images_from_disc([self.image_list[index] for index in indices])
        return image_batch, mask_batch

    def get_test_batch_by_indices(self, indices):
        image_batch, mask_batch = self.read_images_from_disc([self.image_list_test[index] for index in indices])
        return image_batch, mask_batch

    def get_train_batch(self):
        """Returns training images and masks in batch
        Returns:
          Two 4-D numpy arrays: training images and masks in batch.
        """
        print("Training epochs completed:", self.total_epochs + (float(self.current_index) / self.total_images))

        if (self.total_epochs + (float(self.current_index) / self.total_images)) >= self.options.training_epochs:
            return None, None

        self.last_indices = np.random.choice(self.total_images, self.options.batch_size)
        self.all_train_indices.extend(self.last_indices)
        image_batch, mask_batch = self.read_images_from_disc([self.image_list[index] for index in self.last_indices])

        self.current_index = self.current_index + self.options.batch_size
        if self.current_index > self.total_images:
            self.current_index = self.current_index - self.total_images
            self.total_epochs = self.total_epochs + 1

        return image_batch, mask_batch

    def get_test_batch(self):
        """Returns testing images and masks in batch
        Args:
          None
        Returns:
          Two 4-D numpy arrays: test images and masks in batch.
        """
        # Optional Image and Label Batching
        self.last_indices = np.random.choice(self.total_images_test, self.options.batch_size)
        self.all_test_indices.extend(self.last_indices)
        image_batch, mask_batch = self.read_images_from_disc([self.image_list_test[index] for index in self.last_indices])
        return image_batch, mask_batch

    def get_random_test_batch(self):
        all_images = []

        for v in self.image_test_list.values():
            all_images.extend(v)

        random_images = random.sample(all_images, self.options.batch_size)
        image_batch, mask_batch = self.read_images_from_disc(random_images)
        return image_batch, mask_batch

    def get_ordered_test_batch(self, image_source):
        current_index = 0
        image_list = self.image_test_list[image_source]
        indices = self.source_indices[image_source]

        if indices:
            current_index = indices[-1] + 1

        end_index = current_index+self.options.batch_size

        if end_index > len(image_list):
            return None, None, None

        image_data = image_list[current_index:end_index]
        image_batch, mask_batch = self.read_images_from_disc(image_data)
        indices.extend(range(current_index, end_index))
        self.tested_images.extend(image_data)

        return image_batch, mask_batch, image_data

    def iterate_ordered_test_batches(self, image_source, max_images=None):
        image_batch, mask_batch, image_data = self.get_ordered_test_batch(image_source)
        consumed_images = 0

        while image_batch is not None and mask_batch is not None and image_data is not None:
            yield image_batch, mask_batch, image_data

            consumed_images += len(image_batch)
            if max_images is not None and consumed_images + self.options.batch_size > max_images:
                break

            image_batch, mask_batch, image_data = self.get_ordered_test_batch(image_source)

    def get_source_images(self, source, max_count=10):
        data = self.image_test_list[source]
        if len(data) > max_count:
            data = random.sample(data, max_count)
        images = self.read_images_from_disc(data)[0]
        return images

    def get_image_sources(self):
        return self.image_test_list.keys()

    def restore_checkpoint(self, num_steps):
        """Restores current index and epochs using numSteps
        Args:
          num_steps: Number of batches processed
        Returns:
          None
        """
        processed_images = num_steps * self.options.batch_size
        self.total_epochs = processed_images / self.total_images
        self.current_index = processed_images % self.total_images

    def get_train_progress_image(self, session_path):
        return os.path.join(session_path, TRAIN_PROGRESS_DIRECTORY, 'img.png')

    def get_test_progress_image(self, session_path):
        return os.path.join(session_path, TEST_PROGRESS_DIRECTORY, 'img.png')

    def get_train_progress_labels(self, session_path):
        labels = util.get_image_paths(os.path.join(session_path, TRAIN_PROGRESS_DIRECTORY), ignore='img.png')
        labels.sort(key=os.path.getmtime)
        return labels

    def get_test_progress_labels(self, session_path):
        labels = util.get_image_paths(os.path.join(session_path, TEST_PROGRESS_DIRECTORY), ignore='img.png')
        labels.sort(key=os.path.getmtime)
        return labels
