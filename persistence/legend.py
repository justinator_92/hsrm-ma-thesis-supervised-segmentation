import numpy as np
import skimage.io
import skimage.color
import warnings
from reportlab.lib.units import mm
from reportlab.platypus.flowables import Flowable
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.lib.utils import ImageReader
import io


def filled_box(color):
    """ Creates a color filled box """
    color_array = np.full((50, 50, 3), color, dtype=np.uint8)
    image_mem = io.BytesIO()
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        skimage.io.imsave(image_mem, color_array)
    image_mem.seek(0)
    return image_mem


class Legend(Flowable):
    """ Color legend """

    def __init__(self, labels, colors):
        Flowable.__init__(self)
        self.x_offset = 37 * mm
        self.size = 10 * mm

        self.labels = labels
        self.colors = colors

    def wrap(self, *args):
        return self.x_offset, self.size

    def draw(self):
        canvas = self.canv

        box_size = 3 * mm
        start = 0
        for label, color in zip(self.labels, self.colors):
            text = canvas.beginText(start, 0)
            text.setFont("Helvetica", 11)
            text.textLine(label)
            canvas.drawText(text)

            w = int(stringWidth(label, "Helvetica", 11))
            box_start = start + w + 0.5 * mm
            canvas.drawImage(ImageReader(filled_box(color)), box_start, 0, box_size, box_size)

            start = box_start + box_size + 1 * mm
