import os
import datetime as dt

from collections import namedtuple


class ConfigObject(object):

    def __init__(self, config_file_path, train_path):
        assert os.path.exists(config_file_path), "File:" + config_file_path + " does not exists"

        with open(config_file_path) as f:
            content = f.readlines()
        assert content is not None

        self.options_dic = self.__parse_content(content)
        self['session_timestamp'] = dt.datetime.now().isoformat()  # add current session timestamp

        # construct output directory for current data set
        session_path = os.path.basename(os.path.normpath(train_path))
        session_path = session_path.replace('_test', '').replace('_train', '')
        self['session_directory'] = os.path.join(self['output_directory'], session_path)

    def __parse_content(self, content):
        key_dict = {}

        content = map(str.strip, content)  # drop all empty lines..
        content = filter(lambda v: not (v.startswith('#') or v == ""), content)  # drop comments and empty lines..

        for line in content:
            line_split = list(map(str.strip, line.split("=")))
            assert (len(line_split) == 2), "value " + line_split + "is not a key-value pair!"

            key, value = line_split[0], line_split[1]
            assert (key not in key_dict)

            key_dict[key] = self.__parse_value(value)

        return key_dict

    def __parse_value(self, value):
        for type_cast in [int, float]:  # check for numeric values...
            try:
                ret_value = type_cast(value)
                return ret_value

            except ValueError:  # move on...
                continue

        # check for booleans...
        if value == "True":
            return True
        if value == "False":
            return False
        # check for NoneType
        if value == "None":
            return None

        return value  # return as string...

    def __setitem__(self, key, value):
        assert (type(key) == str)

        if key in self.options_dic:
            print("update values:", key, value)
            assert (type(value) == type(self.options_dic[key])), value  # keep the same type of values...

        self.options_dic[key] = value

    def __getitem__(self, item):
        print("get", item)
        assert (item in self.options_dic), item + " is not in options!"
        return self.options_dic[item]

    def __repr__(self):
        key_value_pairs = map(lambda v: v[0] + ": " + str(v[1]), self.options_dic.items())
        return "ConfigObject: {" + str.join(", ", key_value_pairs) + "}"

    def parse_configuration(self):
        return namedtuple('OptionsDict', self.options_dic.keys())(**self.options_dic)


if __name__ == '__main__':
    config_dir = os.path.join(os.path.abspath(os.curdir), '../config/config.cfg')
    o = ConfigObject(config_dir, "hello/dir")
    o["epochs"] = 10
    o["time"] = 'test'

    c = o.parse_configuration()
    print("timestamp", c.session_timestamp)
    print(c)