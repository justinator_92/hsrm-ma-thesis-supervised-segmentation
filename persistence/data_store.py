import os
import skimage
import numpy as np
from persistence import util


class DataStore(object):

    def __init__(self, options):
        self.options = options

    def create_session_directories(self):
        # create root and session directory
        if not os.path.exists(self.options.output_directory):
            os.mkdir(self.options.output_directory)

        if not os.path.exists(self.options.session_directory):
            os.mkdir(self.options.session_directory)

        for path in [self.options.train_progress_path, self.options.test_progress_path,
                     self.options.train_eval_directory, self.options.test_eval_directory,
                     self.options.data_directory, self.options.loss_directory]:
            # create full path to session directory
            dir_path = os.path.join(self.options.session_directory, path)

            if os.path.exists(dir_path):
                os.system("rm -rf " + dir_path)
            os.mkdir(dir_path)

        print("Removing previous checkpoints and logs")
        os.system("rm -rf " + self.options.checkpoint_dir)
        os.system("rm -rf " + self.options.logs_dir)
        os.system("mkdir " + self.options.checkpoint_dir)

    def prepare_info_files(self):
        info_path = os.path.join(self.options.session_directory,
                                 self.options.loss_directory,
                                 self.options.session_timestamp + '.info')

        info_file = open(info_path, 'w+')
        info_file.write("batch_size=%s" % self.options.batch_size + '\n' +
                        "learning_rate=%s" % self.options.learning_rate)
        info_file.close()

        # ... and corresponding loss files
        loss_path_train = os.path.join(self.options.session_directory,
                                       self.optionsloss_directory,
                                       self.options.session_timestamp + '.train')

        _loss_file = open(loss_path_train, 'w+')
        _loss_file.close()

        loss_path_test = os.path.join(self.options.session_timestamp,
                                      self.options.loss_directory,
                                      self.options.session_timestamp + '.test')

        _loss_file = open(loss_path_test, 'w+')
        _loss_file.close()

    def save_initial_progress_image(self, image, is_train=True):
        directory = self.options.train_progress_path if is_train else self.options.test_progress_path
        path = os.path.join(self.options.session_directory, directory, 'img.png')
        skimage.io.imsave(path, image)

    def save_progress_outputs(self, output_images, suffix=0, is_train=True):
        directory = self.options.train_progress_path if is_train else self.options.test_progress_path

        img = util.convert_one_hot_to_gray_image(output_images[0])  # take first image from batch...
        small_image = skimage.transform.resize(img, (100, 100), preserve_range=True).astype(np.uint8)

        img_name = 'label_' + str(suffix) + '.png'
        path = os.path.join(self.options.session_directory, directory, img_name)

        skimage.io.imsave(path, small_image)
