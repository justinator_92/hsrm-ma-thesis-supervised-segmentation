import argparse
import skimage.io
import numpy as np
import os

CHANNELS = 1


def compute_means(image_paths):
    sums = np.zeros(CHANNELS)
    total = 0

    for image_path in image_paths:
        img = skimage.io.imread(image_path)
        sums += np.sum(np.sum(img, axis=0), axis=0)
        total += img.shape[0] * img.shape[1]

    means = sums / total
    return means, total


def compute_standard_deviations(image_paths, means, total):
    deviation_squares = np.zeros(CHANNELS)

    image_paths = sorted(image_paths)
    for image_path in image_paths:
        img = skimage.io.imread(image_path)
        img = img.astype(np.float64)

        img -= means
        squared_img = np.square(img)
        deviation_squares += np.sum(np.sum(squared_img, axis=0), axis=0)

    variances = deviation_squares / total
    standard_deviations = np.sqrt(variances)
    return standard_deviations


def compute(images_path, stats_path):
    image_files = filter(lambda x: "gray" in x, os.listdir(images_path))  # drop mask image files..
    image_files = sorted(map(lambda x: os.path.join(images_path, x), image_files))  # make full file name..

    means, total = compute_means(image_files)
    standard_deviations = compute_standard_deviations(image_files, means, total)

    # BGR ordering is required
    means = means[::-1]
    standard_deviations = standard_deviations[::-1]

    if CHANNELS == 1:  # stack means
        means = np.array([means[0], means[0], means[0]])
        standard_deviations = np.array([standard_deviations[0], standard_deviations[0], standard_deviations[0]])

    print("means:", means)
    print("standard deviation:", standard_deviations)
    print("Save to stats...")
    np.savetxt(stats_path, np.array([means, standard_deviations]))


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()

    # Single argument
    arg_parser.add_argument('--imagesPath', type=str, default='')
    arg_parser.add_argument('--statsPath', type=str, default='stats.txt')
    # Parse args
    args = arg_parser.parse_args()
    print("Arguments: %s" % args)

    compute(args.imagesPath, args.statsPath)
